import React, { useState } from "react";
import { useDropzone } from "react-dropzone";

const Dropzone = ({ onDrop, accept }) => {

	const { getRootProps, getInputProps, isDragActive, acceptedFiles } = useDropzone({ onDrop, accept });
	const [fileName, setFileName] = useState();
	const getClassName = (className, isActive) => {
		if (!isActive) return className;
		return `${className} ${className}-active`;
	};

	const AddedFiles = () => (
		<span className="xl-file-name">{acceptedFiles.map(f => setFileName(f.name))}</span>
	)

	return (
		<section className="container">
			<div className="dropzone-area" {...getRootProps()}>
				<input className="dropzone-input" {...getInputProps()} />
				<div className="text-center ">
					{isDragActive ? (
						<p className="dropzone-content">Release to drop the files here</p>
					) : (
							<div className="dropzone-content">
								<span className="dropzone-icon"></span>
								<span className={`drag-text ${fileName ? 'active' : ''}`}>{fileName ? fileName : "Drag & Drop File"}</span>
							</div>
						)}
				</div>

				<div className={getClassName("dropzone", isDragActive)} {...getRootProps()}></div>
			</div>

			<AddedFiles />

		</section>
	);
};

export default Dropzone;