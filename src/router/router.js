import AdminDashboard from 'modules/admin/Dashboard';
import WholesellerDashboard from 'modules/wholeseller/Dashboard';
import TagList from 'modules/tag/TagList';
import TagRequest from 'modules/tag/TagRequest';
import AssignTag from 'modules/tag/AssignTag';

const Router = [
    { path: '/admin-dashboard', component: AdminDashboard, exact: true},
    { path: '/wholeseller-dashboard', component: WholesellerDashboard, exact: true},
    { path: '/tag-list', component: TagList, exact: true},
    { path: '/tag-request', component: TagRequest, exact: true},
    { path: '/assign-tag', component: AssignTag, exact: true},
]

export default Router