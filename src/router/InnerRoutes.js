import React from 'react';
import { Switch } from "react-router-dom";
import Router from "./Router";
import PrivateRoute from './PrivateRoute';

/**
 * Lazy Load page
 */
const PageNotFound = React.lazy(() => import('components/PageNotFound'));

const InnerRoutes = ({ appState }) => {

    return (
        <Switch>
            {
                Router.map((route, index) => {
                    if (route.component) {
                        return <PrivateRoute key={index} path={route.path} exact={route.exact} component={route.component} />
                    }
                    return null
                })
            }
            <PrivateRoute component={PageNotFound} />
        </Switch>
    )
}
export default InnerRoutes;