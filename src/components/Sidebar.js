import React, { useContext } from 'react';
import '../assets/scss/sidebar.scss';
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

const Sidebar = ({ sidebarPara, appState }) => {

    return (
        <React.Fragment>
            
            <nav id="sidebar" className={`sidebar ${sidebarPara.sidebar ? "active" : ""}`}>
                <div className="sidebar-navigation">
                    <button type="button" className="humburger-button" onClick={sidebarPara.overlaySidebar}>
                        <img alt="" src={require(`../assets/images/sidebar/humburger.svg`)} className="humburger-logo" />
                    </button>
                    <div className="navigation-text"></div>
                </div>
                <ul className="sidebar-list">
                {appState.user.user_role == 'SUPER_ADMIN' &&        
                <li className="sidebar-item">
                        <NavLink activeClassName='link-active' to="/admin-dashboard" onClick={sidebarPara.overlaySidebar} className="side-icon dashboard">
                            Wholesellers
                        </NavLink>
                    </li>      
                    }
                    

                {appState.user.user_role == 'WHOLE_SELLER' &&    
                <li className="sidebar-item">
                    <NavLink activeClassName='link-active' to="/wholeseller-dashboard" onClick={sidebarPara.overlaySidebar} className="side-icon dashboard">
                        Retailers
                    </NavLink>
                </li>
                }

                {appState.user.user_role == 'WHOLE_SELLER' &&    
                <li className="sidebar-item">
                    <NavLink activeClassName='link-active' to="/tag-list" onClick={sidebarPara.overlaySidebar} className="side-icon reports">
                        Tag List
                    </NavLink>
                </li>
                }

                {appState.user.user_role == 'WHOLE_SELLER' &&    
                <li className="sidebar-item">
                    <NavLink activeClassName='link-active' to="/tag-request" onClick={sidebarPara.overlaySidebar} className="side-icon templates">
                        Tag Requests
                    </NavLink>
                </li>
                }

                {appState.user.user_role == 'WHOLE_SELLER' &&    
                <li className="sidebar-item">
                    <NavLink activeClassName='link-active' to="/assign-tag" onClick={sidebarPara.overlaySidebar} className="side-icon themes">
                        Assign Tags
                    </NavLink>
                </li>
                }
                </ul>
                <div className="copyright-text">
                    © {new Date().getFullYear()} Great Place IT Services ™. <br />All Rights Reserved.
                </div>
            </nav>
        </React.Fragment>
    )
}
const mapStateToProps = state => {
    //console.log(state.app.appState.user.user_role);
    return {
        appState: state.app.appState,
    }
}
export default connect(mapStateToProps)(Sidebar);

