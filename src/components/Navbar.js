import React, { useContext } from 'react';
import '../assets/scss/navbar.scss';
import { Dropdown } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import AppContext from 'store/AppContext';
import { connect } from "react-redux";
import * as AppActions from "store/actions";

const Navbar = ({ navbarPara, appState, logOutUser }) => {

    const history = useHistory(); 
    const { languageObj } = useContext(AppContext);

    /**
     * Logout User
     */
    const handleLogout = () => {
        localStorage.removeItem('isLogin');
        localStorage.removeItem('appState');
        logOutUser()
        history.push("/");
    }

    return (
        <React.Fragment>
            <header className="header">
                <div className="menu-wrap">
                    <div className="logo-wrap">
                        <div className="humberger">
                            <div className="humberger-inner" onClick={navbarPara.sidebarCollapse}>
                                <img alt="" src={require(`../assets/images/sidebar/header-humburger.svg`)} className="header-humburger-icon" />
                            </div>
                        </div>
                        <div className="logo">
                            <img alt="" src={require(`../assets/images/logo.png`)} className="logo-img" />
                        </div>
                    </div>
                    <div className="notification-info">
                        <div className="notification">
                            <span className="noti-icon"></span>
                        </div>
                        <div className="user-info">
                            <div className="user-detail">
                                <label>{(appState.user && appState.user.name) || "User"}</label>
                                <span>{(appState.current_app_detail && appState.current_app_detail.name) || "Unknown"}</span>
                            </div>
                            <div className="userInfoDropdown">
                                <Dropdown alignRight>
                                    <Dropdown.Toggle id="dropdown-basic">
                                        <div className="user-avtar">
                                            <img className="avtar" alt="" src={(appState && appState.user && appState.user.profile_pic_url) ? appState.user.profile_pic_url : require(`../assets/images/user.png`)} />
                                            <span className="test"></span>
                                        </div>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={handleLogout}><span className="user-indication">{languageObj.translate('Not.1')} {(appState.user && appState.user.name) || "User"}?</span> {languageObj.translate('SignOut.1')}</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </div>
                    <div className="responsive-menu-wrap">
                        <span className={navbarPara.mobMenu ? "active" : ""}></span>
                        <button type="button" className="menu-button" onClick={navbarPara.mobileMenu}><span className="menu-icon"></span></button>
                    </div>
                </div>
            </header>
        </React.Fragment>
    )
}

const mapStateToProps = state => {
    return {
        appState: state.app.appState,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logOutUser: () => dispatch(AppActions.logOutUser())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);