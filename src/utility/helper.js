
export const dateConvert = (date = null) => {
    let currentdate = date || new Date()
    return currentdate.getFullYear() + "-"
        + (currentdate.getMonth() + 1) + "-"
        + currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
}

export const getFirstWord = (str) => {
    if (str && str.length !== null) {
        let spaceIndex = str.replace(/ .*/, '');
        return spaceIndex;
    }
};

export const toTitleCase = (str) => {
    if (str && str.length !== null) {
        return str.replace(/\w\S*/g, (txt) => {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
}

export const outlineRemove = {
    control: base => ({
        ...base,
        border: 0,
        boxShadow: "none"
    })
};

export const getNameInitials = string => {
    const names = string.split(' ');
    const initials = names.map(name => name.charAt(0).toUpperCase())
    if (initials.length > 1) {
        return `${initials[0]}${initials[initials.length - 1]}`;
    } else {
        return initials[0];
    }
}

export const compareObject = (obj1, obj2) => {
    for (var p in obj1) {
        if (obj1.hasOwnProperty(p) !== obj2.hasOwnProperty(p)) return false;

        switch (typeof (obj1[p])) {
            case 'object':
                if (!compareObject(obj1[p], obj2[p])) return false;
                break;
            case 'function':
                if (typeof (obj2[p]) == 'undefined' || (p != 'compareObject' && obj1[p].toString() != obj2[p].toString())) return false;
                break;
            default:
                if (obj1[p] != obj2[p]) return false;
        }
    }
    for (var p in obj2) {
        if (typeof (obj1[p]) == 'undefined') return false;
    }
    return true;
};

export const uniqueGenerator = () => {
    let date = Date.now();
    if (date <= uniqueGenerator.previous) {
        date = ++uniqueGenerator.previous;
    } else {
        uniqueGenerator.previous = date;
    }

    return 'unq' + date.toString(20) + Math
        .floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

export const surveyQuestionModifier = (questions = {}) => {
    let questionsData = questions.pages || [];
    questionsData.forEach((page, index) => {
        if (questionsData[index] && questionsData[index].hasOwnProperty("elements") && questionsData[index]["elements"].length > 0) {
            questionsData[index]["elements"].forEach((item, i) => {
                if (questionsData[index]["elements"][i]["type"] === 'matrix') {
                    if (questionsData[index]["elements"][i].hasOwnProperty("rows")) {
                        questionsData[index]["elements"][i]["rows"].forEach((mtItem, inum) => {
                            let mtOptions = {}
                            if (typeof mtItem === 'string') {
                                mtOptions['text'] = mtItem
                                mtOptions['value'] = uniqueGenerator()
                            }
                            if (typeof mtItem === 'object') {
                                mtOptions['text'] = mtItem['text']
                                mtOptions['value'] = uniqueGenerator()
                            }
                            questionsData[index]["elements"][i]["rows"][inum] = mtOptions
                        })
                    }
                }
            })
        }
    })
    questions.pages = questionsData
    return questions
}