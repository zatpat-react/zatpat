import axios from "axios";
import { toast } from 'react-toastify';
import { APP_FIELD_NAME, APP_ID } from "constants/constants";

let notificationTimer = null;
const Axios = axios.create();
/**
 * Here Modify request data if needed like Login token, company ids
 */
Axios.interceptors.request.use(request => {
    const appStore = JSON.parse(localStorage.getItem("appState"));
    if (appStore && appStore.user && appStore.user !== null) {
        let { auth_token = "", company_id = "", id = "", partner_id = "" } = appStore.user;
        request.headers["Authorization"] = `Bearer ${auth_token}`;
    } else {
        clearTimeout(notificationTimer);
        notificationTimer = setTimeout(() => {
            toast.warn("Login Expired please login again to continue.");
            window.location = "/"
        }, 100);
    }
    return request;
});

/**
 * Intercept Responses and check if response status os OK.
 */
Axios.interceptors.response.use(response => {
    /**
     * Added temperory solution to check expired data.
     */
    return response;
}, error => {
    clearTimeout(notificationTimer);
    if (!error.response) {
        notificationTimer = setTimeout(() => {
            toast.warn(error.toString() + "Requested resources Could not be loaded")
        }, 300);
    }

    if (error.response && error.response.status) {
        const { data = {}, status = 500 } = error.response
        switch (status) {
            case 401:
                notificationTimer = setTimeout(() => {
                    toast.warn(data.message || "Something went wrong Please try later!!", {
                        onClose: () => {
                            window.location = "/"
                        }
                    });
                }, 300);
                break;
            case 500:
                notificationTimer = setTimeout(() => {
                    toast.error(data.message || "Something went wrong Please try later!!");
                }, 300);
                break;
        }
    }
    return Promise.reject(error);
});

export default Axios;
