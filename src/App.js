import React, { useState } from 'react';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
import useSidebarHooks from './hooks/SidebarNavbarHook';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastContainer, toast, Zoom } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import './App.css';
import './assets/scss/variable.scss';
import './App.scss';
import './assets/font/NunitoSans/NunitoSans.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { useTranslation } from 'react-i18next';
import AppContext from './store/AppContext';
import LANGUAGES from "constants/languages";
import InnerRoutes from 'router/InnerRoutes';
import { appAccess } from 'FeatureAccess';
import EziLoader from 'components/EziLoader';
import { connect } from "react-redux";

const Login = React.lazy(() => import('modules/shared/Login/CompanyLogin'));
const ForgotPassword = React.lazy(() => import('modules/shared/ForgetPassword/ForgotPassword'));

function App({ appState }) {
	/**
	 * Initial App state for login, ACL Matrix
	 */
	const [translate, setTranslate] = useTranslation();
	const [isLoading, setIsLoading] = useState(false);
	const sidebarData = useSidebarHooks();

	/**
	 * Global Loader
	 */
	const globalLoader = () => {
		return {
			show: () => {
				setIsLoading(true)
			},
			hide: () => {
				setIsLoading(false)
			}
		}
	}

	/**
	 * Notify Access warning
	 */
	const handleUnAuthWarn = () => {
		toast.warn("You are not authorized to access this feature!")
	}

	/**
	 * Initial Language Data.
	 */
	const languageObj = {
		curLang: localStorage.getItem("i18nextLng"),
		languages: LANGUAGES,
		changeLang: (langCode = "En") => setTranslate.changeLanguage(langCode),
		translate: translate
	}

	return (
		<AppContext.Provider value={{ appState, languageObj, handleUnAuthWarn, accesFeature: appAccess(appState.access_matrix), EziLoader: globalLoader() }}>
			<Router>
				<Switch>
					<Route exact path="/" render={props => <Login {...props} />} />
					<Route exact path="/reset-password" component={ForgotPassword} />
					<React.Fragment>
						<div className="main-wrapper">
							<Sidebar sidebarPara={sidebarData} />
							<Navbar navbarPara={sidebarData} />
							<div className="inner-wrapper">
								<InnerRoutes />
							</div>
							<div className={`overlay ${sidebarData.sidebar ? "active" : ""}`} onClick={sidebarData.overlaySidebar} ></div>
						</div>
					</React.Fragment>
				</Switch>
			</Router>
			<ToastContainer transition={Zoom} position={toast.POSITION.BOTTOM_RIGHT} closeButton={false} hideProgressBar={true} />
			{isLoading && <EziLoader />}
		</AppContext.Provider>
	);
}

const mapStateToProps = state => {
	return {
		appState: state.app.appState,
		language: state.app.language,
	}
}

export default connect(mapStateToProps)(App);