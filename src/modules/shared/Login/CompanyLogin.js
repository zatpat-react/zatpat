import React, { useContext, useState, useEffect } from 'react';
import './CompanyLogin.scss';
import { Dropdown, Spinner } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import Axios from 'axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import useForm from 'react-hook-form';
import { APP_ID, APP_FIELD_NAME } from "constants/constants"
import { connect } from "react-redux";
import * as AppActions from "store/actions";

const Login = (props) => {

	const { languageObj } = useContext(AppContext);
	const [loader, setLoader] = useState({ otp: false, login: false });
	const [otpLogin, setOtpLogin] = useState(false);
	const [optActed, setOptActed] = useState(false);
	const [viewPwd, setViewPwd] = useState(false);
	const { register, handleSubmit, errors, triggerValidation } = useForm()

	/**
	 * Login User
	 * 
	 * @param {target} 
	 */
	const handleLogin = (data) => {
		if (!optActed) {
			handleSubmitForm(data)
		}
	}

	/**
	 * Clear login Data.
	 */
	useEffect(() => {
		localStorage.removeItem("appState")
		localStorage.removeItem("isLogin")
		props.logOutUser()
	}, [])

	/**
	 * Login With username and password
	 * 
	 * @param {Object} data 
	 */
	const handleSubmitForm = (data = {}) => {
		let formData = new FormData();
		formData.append("username", data.username);
		formData.append("password", data.password);
		setLoader({ ...loader, login: true })
		Axios.post(configURL.loginURL, formData).then(response => {
			setLoader({ ...loader, login: false })
			if (response.data.success !== undefined && response.data.success === true) {
				let userData = {
					name: response.data.user.first_name,
					first_name: response.data.user.first_name,
					last_name: response.data.user.last_name,
					mobile: response.data.user.mobile,
					email: response.data.user.email,
					auth_token: response.data.token,
					status: response.data.user.status,
					user_role: response.data.user.user_role
				};
				localStorage.setItem("appState", JSON.stringify({
					isLoggedIn: true,
					user: userData,
				}));

				localStorage.setItem('isLogin', true);
				props.loginUser()
				if (response.data.user.user_role == 'SUPER_ADMIN') {
					props.history.push("/admin-dashboard");
				} else {
					props.history.push("/wholeseller-dashboard");
				}
			} else {
				toast.warn(response.data.message);
			}
		}).catch(error => {
			setLoader({ ...loader, login: false })
			let messageWrong = "Something went wrong Please try later!!"
			let messageIntr = "Please check your Internet connection!"
			if (!error.response) {
				toast.warn(messageIntr);
			}
			if (error.response && error.response.status) {
				const { data = {} } = error.response
				toast.warn(data.message || messageWrong)
			}
		})
	}

	/**
	 * Trigger validation on OTP
	 */
	const validateUserName = () => {
		triggerValidation("username");
		setOptActed(true);
	}

	return (
		<div className="bg-wrapper">
			<div className="login">
				<div className="login-inner">
					<div className="login-bg">
						<img alt="" src={require(`../../../assets/images/login-img.png`)} className="login-img animated fadeIn" />
					</div>
					<div className="login-form animated fadeIn">
						{!otpLogin && <form onSubmit={handleSubmit(handleLogin)}>
							<div className="login-logo">
								<img alt="" src={require(`../../../assets/images/logo.png`)} />
							</div>
							<p className="sign-in">{languageObj.translate("CompanySignIn.1")}</p>
							<div className="input-wrap">
								<input type="text" name="username" className={`login-input-control user-input`} ref={register({
									required: true, pattern: /^([_a-z0-9]+(.[_a-z0-9]+)@[a-z0-9-]+(.[a-z0-9-]+)(.[a-z]{2,5}))|(\d+$)$/
								})} placeholder="Email" />
								{errors.username && errors.username.type === 'required' && <span className={`error-message`}>{languageObj.translate(`loginValidation.1.empty.username`)}</span>}
								{errors.username && errors.username.type === 'pattern' && <span className={`error-message`}>{languageObj.translate(`loginValidation.1.invalid.username`)}</span>}
							</div>

							<div className="input-wrap">
								<input type={`${viewPwd ? "text" : "password"}`} name="password" className={`login-input-control password-input`} ref={register({
									required: !optActed
								})} placeholder={languageObj.translate('Password.1')} onChange={() => setOptActed(false)} />
								<button type="button" className="ezi-btn visibility-button" onClick={() => setViewPwd(!viewPwd)}>
									<span className={`eye_icon ${viewPwd ? "visible_on" : "visiblke_off"}`}></span>
								</button>
								{errors.password && <span className={`error-message`}>{languageObj.translate(`loginValidation.1.empty.password`)}</span>}
							</div>
							<div className="otp-btn-wrap">
								<button type="submit" className={`ezi-btn company-login-btn btn-ripple`} onClickCapture={() => setOptActed(false)}>
									{languageObj.translate("SignIn.1")}
									{loader.login && <Spinner animation="grow" />}
								</button>
							</div>
						</form>}
						<div className="forgot-wrapper">
							<label className="login-checkbox">{languageObj.translate("KeepMeLoggedIn.1")}
								<input type="checkbox" />
								<span className="checkmark"></span>
							</label>
							<div className="forgot-text"><a>{languageObj.translate("ForgotPassword.1")}</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

const mapDispatchToProps = dispatch => {
	return {
		loginUser: () => dispatch(AppActions.loginUser()),
		logOutUser: () => dispatch(AppActions.logOutUser())
	}
}

export default connect(null, mapDispatchToProps)(Login);