import React, { useContext, useState } from 'react';
import 'modules/shared/Login/CompanyLogin.scss';
import { Spinner } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import useForm from 'react-hook-form'
import Axios from 'axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';

const ForgotPassword = (props) => {

    const { languageObj } = useContext(AppContext);
    const { register, handleSubmit, errors, watch } = useForm()
    const [loader, setLoader] = useState(false);
    const onSubmit = data => console.log(data)

    return (
        <div className="bg-wrapper">
            <div className="login">
                <div className="login-inner">
                    <div className="login-bg">
                        <img alt="" src={require(`../../../assets/images/login-img.png`)} className="login-img animated fadeIn" />
                    </div>
                    <div className="login-form animated fadeIn">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="login-logo">
                                <img alt="" src={require(`../../../assets/images/logo.png`)} />
                            </div>
                            <p className="sign-in">{languageObj.translate("ChangePassword.1")}</p>
                            <div>

                                <div className="input-wrap">
                                    <input type="text" name="otpText" className={`login-input-control user-input`} ref={register({ required: true })} />
                                    {errors.otpText && <span className={`error-message`}>{languageObj.translate("resetPwd.1.otpText")}</span>}
                                </div>
                                <div className="input-wrap">
                                    <input type="text" name="password" className={`login-input-control user-input`} ref={register({ required: true, minLength: 8 })} />
                                    {errors.password && errors.password.type === 'required' && <span className={`error-message`}>{languageObj.translate("resetPwd.1.confirm_password")}</span>}
                                    {errors.password && errors.password.type === 'minLength' && <span className={`error-message`}>{languageObj.translate("resetPwd.1.minLength")}</span>}
                                </div>
                                <div className="input-wrap">
                                    <input type="password" name="confirm_password" className={`login-input-control password-input`} ref={register({ required: true, validate: (value) => value === watch('password') || languageObj.translate("resetPwd.1.confirm_password") })} />
                                    {errors.confirm_password && errors.confirm_password.type === 'required' && <span className={`error-message`}>{languageObj.translate("resetPwd.1.confirm_password")}</span>}
                                    {errors.confirm_password && errors.confirm_password.type === 'validate' && <span className={`error-message`}>{languageObj.translate("resetPwd.1.notSame")}</span>}
                                </div>
                                <div className="otp-btn-wrap">
                                    <button type="submit" className={`ezi-btn company-login-btn btn-ripple`}>
                                        {languageObj.translate("SendOtp.1")}
                                        {loader.otp && <Spinner animation="grow" />}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ForgotPassword;