import React from 'react';


const GreetUser = () => {
    let local = JSON.parse(localStorage.getItem('appState'));
    let userName = "", firstName = "";
    if (local !== null) {
        userName = local["user"]["name"];
        firstName = userName
    }
    return (
        <div className="greet-text-section">
            <h5 className="page-heading">Welcome {firstName}</h5>
        </div>
    )
}

export default GreetUser;