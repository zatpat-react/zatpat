import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import 'utility/i18next.js';
import { toast } from 'react-toastify';
import BootstrapTable from "react-bootstrap-table-next";
import Axios from "utility/Axios";
import configURL from 'config/config';
import EziLoader from 'components/EziLoader';
import { confirmAlert } from 'react-confirm-alert';
const AssignTag = () => {
  const [translate, setTranslate] = useTranslation();
  const [tags, setTags] = useState([]);
  const [loading, setLoading] = useState(false);

  const actionFormatter = (cell, row) => {
    return (
      <div className="datatable-editDelete-wrap">
        <button type="button" className="datatable-delete_c"  >Assign Tag</button>
      </div>
    );
  }
  const actionHeader = (cell, row) => {
    return (
      <span className="datatable-action-text">Action</span>
    );
  }
  const columns = [
    { dataField: "retailer_name", text: "Retailer" },
    { dataField: "tag_count", text: "Tag Count" },
    { dataField: "action", text: "Action",headerFormatter: actionHeader , formatter: actionFormatter },
  ];
  /* const deleteEntry = (rowId) => {
    confirmAlert({
        title: 'Delete Wholeseller',
        message: `Are you sure you want  to delete this entry ?`,
        buttons: [
            {
                label: 'Okay',
                onClick: () => {
                    if (rowId !== null) {
                        let formData = new FormData();
                        formData.append("id", rowId);
                        Axios.delete(configURL.WHOLESELLERS+'/'+rowId, formData).then(resposnse => {
                            if (resposnse !== undefined && resposnse.status) {
                              alert("User Deleted Successfully.");
                              setTagsListing();
                            }
                        });
                    }
                }
            },
            { label: 'Cancel' }
        ]
    });
} */

const setTagsListing = () => {
    setLoading(true) 
    Axios.get(configURL.ASSIGNEDTAGLIST).then(res => {
      setLoading(false) 
      if (res.data.success !== undefined && res.data.success) {
        setTags(res.data.tagTransactions);
      } else {
        setTags([]);
      }
    })
  };
  
  useEffect(() => {
    let successStatus = sessionStorage.getItem('isShow');
    if (successStatus !== null && successStatus === 'true') {
      toast.success(translate('LoggedIn.1'), {
      });
    }
    sessionStorage.removeItem('isShow');
    setTagsListing();
  }, [])

  return (
    <React.Fragment>
      <section className="Page-ApplicationAdminDashboard">
        <div className="admin-dashboard-header">
          <div className="user-welcome-wrap">
            <span className="user-welcome-text">Total Assigned Tags </span>
          </div> 
        </div>

        <div className="application-admin-table-content">
          <BootstrapTable
            keyField = "id"
            data={tags}
            columns={columns}
            rowClasses="datatable-action-row"
          />
        </div>

        {loading && <EziLoader />}
      </section>
    </React.Fragment>
  )
}
export default AssignTag; 