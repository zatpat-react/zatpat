import React, { useState, useEffect } from 'react';
import './Dashboard.scss';
import { useTranslation } from 'react-i18next';
import 'utility/i18next.js';
import { toast } from 'react-toastify';
import BootstrapTable from "react-bootstrap-table-next";
import AddPartnerModal from './components/AddPartnerModal';
import EditPartnerModal from './components/EditPartnerModal';
import EziAlert from 'components/Alert';
import Axios from "utility/Axios";
import configURL from 'config/config';
import EziLoader from 'components/EziLoader';

const CommonBlock = () => {
  const [translate, setTranslate] = useTranslation();
  const [addPartnerModal, setAddPartnerModal] = useState(false);
  const [editPartnerModal, setEditPartnerModal] = useState(false);
  const [confirmation, setConfirmation] = useState(false);
  const [partners, setPartners] = useState([]);
  const [loading, setLoading] = useState(false);


  const deleteCompany = () => {
    //wroite confirm butoon functionality here
  }

  const actionFormatter = (cell, row) => {
    return (
      <div className="datatable-editDelete-wrap">
        <button type="button" className="datatable-edit_c" onClick={() => setEditPartnerModal(true)}>Edit</button>
        <button type="button" className="datatable-delete_c" onClick={() => setConfirmation(true)} >Delete</button>
      </div>
    );
  }

  const actionHeader = (cell, row) => {
    return (
      <span className="datatable-action-text">Action</span>
    );
  }

  let formData = new FormData();
  // formData.append("page", pageno);



  const columns = [
    { dataField: "name", text: "Name" },
    { dataField: "email", text: "Email" },
    { dataField: "phone", text: "Phone no" },
    { dataField: "dob", text: "Date of Birth" },
    { dataField: "address", text: "Address" },
    { dataField: "iskyc", text: "Is KYC" },
    { dataField: "status", text: "Status" },
    { dataField: "action", text: "Action",headerFormatter: actionHeader , formatter: actionFormatter },
  ];

  const setPartnersListing = () => {
    setLoading(true)
    //setEditModalVisible(false);
    // let formData = new FormData();
    // formData.append("category_id", categoryObj.id);
    // formData.append("page", pageno);
    Axios.get(configURL.iam_partners_list, {}).then(res => {
      setLoading(false)
      //setPartners(res.data.data);
      console.log("ressss", res);
      if (res.data.success !== undefined && res.data.success) {
        //setPagination(res.data.pagination);          
        //setPartners(res.data.result);
        setPartners([]);
      } else {
        setPartners([]);
      }
    })
  };

  useEffect(() => {
    let successStatus = sessionStorage.getItem('isShow');
    if (successStatus !== null && successStatus === 'true') {
      toast.success(translate('LoggedIn.1'), {
      });
    }
    sessionStorage.removeItem('isShow');
    setPartnersListing();
  }, [])

  return (
    <React.Fragment>
      <section className="Page-ApplicationAdminDashboard">

        <div className="admin-dashboard-header">
            <button type="button" className="btn-ripple ezi-pink-btn add-new-partner" onClick={() => setAddPartnerModal(true)}>Assigned Tags</button>

            <button type="button" className="btn-ripple ezi-pink-btn add-new-partner" onClick={() => setAddPartnerModal(true)}>Request Tags</button>
        </div>

        {/* <div className="application-admin-table-content">
          <BootstrapTable
            keyField="id"
            data={partners}
            columns={columns}
            rowClasses="datatable-action-row"
          />
        </div> */}

        <AddPartnerModal show={addPartnerModal} onHide={() => setAddPartnerModal(false)} //updatePartnerlisting={() => setPartnersListing()} 
        />
        <EditPartnerModal show={editPartnerModal} onHide={() => setEditPartnerModal(false)} />
        <EziAlert show={confirmation} alerttext="Are you sure you want to delete this ID ?" confirmtext="Confirm" showcancel confirm={deleteCompany} onhide={() => setConfirmation(false)} />
        {loading && <EziLoader />}
      </section>
    </React.Fragment>
  )
}
export default CommonBlock; 