import React, { useState, useEffect } from 'react';
import './TagList.scss';
import { useTranslation } from 'react-i18next';
import 'utility/i18next.js';
import { toast } from 'react-toastify';
import BootstrapTable from "react-bootstrap-table-next";
import AddTagModal from './components/AddTagModal';
import Axios from "utility/Axios";
import configURL from 'config/config';
import EziLoader from 'components/EziLoader';

const TagList = () => {
  const [translate, setTranslate] = useTranslation();
  const [addTagModal, setAddTagModal] = useState(false);
  const [tags, setTags] = useState([]);
  const [loading, setLoading] = useState(false);

  const columns = [
    { dataField: "VehicalType", text: "Vehicle Type" },
    { dataField: "TagCount", text: "Tag Count" },
  ];

const setTagsListing = () => {
    setLoading(true)
    Axios.get(configURL.TAGSLIST, {}).then(res => {
      setLoading(false)
      if (res.data.success !== undefined && res.data.success) {
        setTags(res.data.transactions);
      } else {
        setTags([]);
      }
    })
  };
  
  useEffect(() => {
    let successStatus = sessionStorage.getItem('isShow');
    if (successStatus !== null && successStatus === 'true') {
      toast.success(translate('LoggedIn.1'), {
      });
    }
    sessionStorage.removeItem('isShow');
    setTagsListing();
  }, [])

  return (
    <React.Fragment>
      <section className="Page-ApplicationAdminDashboard">
        <div className="admin-dashboard-header">
          <div className="user-welcome-wrap">
            <span className="user-welcome-text">Tags List </span>
          </div>
           <button type="button" className="btn-ripple ezi-pink-btn add-new-partner" onClick={() => setAddTagModal(true)}>Add Tag</button> 
        </div>

        <div className="application-admin-table-content">
          <BootstrapTable
            keyField = "id"
            data={tags}
            columns={columns}
            rowClasses="datatable-action-row"
          />
        </div>

         <AddTagModal show={addTagModal} onHide={() => setAddTagModal(false)} updateTagslisting={() => setTagsListing()} />

        {loading && <EziLoader />}
      </section>
    </React.Fragment>
  )
}
export default TagList; 