import React from "react";
import { Modal } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import Select from 'react-select';

function AddTagModal(props) {
    const { register, handleSubmit, errors } = useForm();
 
    var options = [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
    ];
    const submitFormData = data => {
        const formData = new FormData();
        formData.append('name', data.name);
        formData.append('email', data.email);
        formData.append('mobile', data.mobile);
        formData.append('aadhar_no', data.aadhar_no);
        formData.append('details', data.details);
        formData.append('status', data.status); 
        Axios.post(configURL.WHOLESELLERS, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                toast.success(res.data.message);
                props.onHide();
                props.updateTagslisting();
            } else {
                toast.error(res.data.message);

            }
        })
    };
    return (
        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="ezi-common-modal-wrapper" >
            <Modal.Body>
                <div className="application-admin-modal-header">
                    <span className="ezi-common-modal-body-close" onClick={props.onHide}></span>
                    <div className="admin-modal-header-text">
                        <span className="title">Add Tags</span>
                    </div>
                </div>
                <form onSubmit={handleSubmit(submitFormData)} className="application-admin-form">
                    <label style= {{ color:"#D9D9D9" }}> Vehicle Type </label>
                    <Select
                            name="vehicle_type"
                            id="vehicle_type"
                            ref={register({ required: true })} required
                            options={options}
                        />

                    <div className="ezi-material-group">
                        <input type="text" name="count" id="count" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">Tag Count</label>
                    </div>

                    <div className="application-admin-form-footer">
                        <input type="submit" value="Save" className="btn-ripple ezi-pink-btn application-admin-save-btn" />
                        <button type="button" className="application-admin-close-btn" onClick={props.onHide}>Close</button>
                    </div>
                </form>

            </Modal.Body>
        </Modal>
    );
}
export default AddTagModal;