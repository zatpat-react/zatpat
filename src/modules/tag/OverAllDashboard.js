import React, { Component, Fragment } from 'react';
import './OverAllDashboard.scss';
import Loader from 'components/EziLoader';
import CommonBlock from "./CommonBlock";
import RequestedTags from "./RequestedTags";
import AssignedTags from './AssignedTags';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import GreetUser from './GreetUser';
import AppContext from 'store/AppContext';
import { toast } from 'react-toastify';


class OverAllDashboard extends Component {
    state = {
        loading: false,
        headerData: null,
        recentCards: null,
    }
    static contextType = AppContext;

    componentDidMount() {
        this.setState({ loading: true })
        Axios.post(configURL.get_dashboard_count_graphs, {}).then(response => {
            if (response.data.success !== undefined && response.data.success == true) {
                this.setState({
                    headerData: response.data.results
                })
            }
        })
        Axios.post(configURL.get_dashboard_survey_highlights, {}).then(response => {
            this.setState({ loading: false })
            if (response.data.success !== undefined && response.data.success == true) {
                this.setState({
                    recentCards: response.data.results
                })
            }
        }).catch(() => {
            this.setState({ loading: false })
        })
    }

    render() {
        const { languageObj = {} } = this.context
        return (
            <Fragment>
                <section className="Page-Dashboard">
                    <div className="column-header">
                        <GreetUser />
                    </div>
                    <CommonBlock commonBlockData={this.state.commonBlockData} />
                    <div className="dashboard-col-wrap">
                        {/* <RecentCards recentCards={this.state.recentCards} /> */}
                        
                        <RequestedTags requestedTagsData={this.state.requestedTagsData} />
                        <AssignedTags assignedTagsData={this.state.assignedTagsData} />
                    </div>
                </section>
                {this.state.loading && <Loader />}
            </Fragment>
        )
    }
}
export default OverAllDashboard; 