import React, { useState, useEffect } from 'react';
import './Dashboard.scss';
import { useTranslation } from 'react-i18next';
import 'utility/i18next.js';
import { toast } from 'react-toastify';
import BootstrapTable from "react-bootstrap-table-next";
import AddRetailerModal from './components/AddRetailerModal';
import EditRetailerModal from './components/EditRetailerModal';
import Axios from "utility/Axios";
import configURL from 'config/config';
import EziLoader from 'components/EziLoader';
import { confirmAlert } from 'react-confirm-alert';

const Dashboard = () => {
  const [editData, seteditData] = useState({});
  const [translate, setTranslate] = useTranslation();
  const [addRetailerModal, setAddRetailerModal] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [editModelData, setEditModelData] = useState({});
  const [confirmation, setConfirmation] = useState(false);
  const [retailers, setRetailers] = useState([]);
  const [loading, setLoading] = useState(false);

const deleteEntry = (rowId) => {
    confirmAlert({
        title: 'Delete Retailer',
        message: `Are you sure you want  to delete this entry ?`,
        buttons: [
            {
                label: 'Okay',
                onClick: () => {
                    if (rowId !== null) {
                        let formData = new FormData();
                        formData.append("id", rowId);
                        Axios.delete(configURL.RETAILERS+'/'+rowId, formData).then(resposnse => {
                            if (resposnse !== undefined && resposnse.status) {
                              alert("User Deleted Successfully.");
                              setRetailersListing();
                            }
                        });
                    }
                }
            },
            { label: 'Cancel' }
        ]
    });
}

const actionFormatter = (cell, row) => {
  return (
    <div className="datatable-editDelete-wrap">
      <button type="button" className="datatable-edit_c" onClick={() => setEditRowData(row)}>Edit</button>
      <button type="button" className="datatable-delete_c" onClick={() => deleteEntry(row.retailer_id)} >Delete</button>
    </div>
  );
}

const actionHeader = (cell, row) => {
  return (
    <span className="datatable-action-text">Action</span>
  );
}

  let formData = new FormData();
  const columns = [
    { dataField: "name", text: "Name" },
    { dataField: "email", text: "Email" },
    { dataField: "mobile", text: "Phone no" },
    { dataField: "aadhar_no", text: "UID" },
    { dataField: "details", text: "Address" },
    { dataField: "status", text: "Status" },
    { dataField: "action", text: "Action",headerFormatter: actionHeader , formatter: actionFormatter },
  ];

const setRetailersListing = () => {
    setLoading(true)
    setEditModalVisible(false);
    Axios.get(configURL.RETAILERS, {}).then(res => {
      setLoading(false)
      if (res.data.success !== undefined && res.data.success) {
        setRetailers(res.data.retailers);
      } else {
        setRetailers([]);
      }
    })
  };
  const toggleEditRetailerModal = () => {
    setEditModalVisible(!editModalVisible);
  }

  const setEditRowData = (rowData) => {
      toggleEditRetailerModal();
      setEditModelData(rowData);
  }
  useEffect(() => {
    let successStatus = sessionStorage.getItem('isShow');
    if (successStatus !== null && successStatus === 'true') {
      toast.success(translate('LoggedIn.1'), {
      });
    }
    sessionStorage.removeItem('isShow');
    setRetailersListing();
  }, [])

  return (
    <React.Fragment>
      <section className="Page-ApplicationAdminDashboard">
        <div className="admin-dashboard-header">
          <div className="user-welcome-wrap">
            <span className="user-welcome-text">Retailers List </span>
          </div>
          <button type="button" className="btn-ripple ezi-pink-btn add-new-partner" onClick={() => setAddRetailerModal(true)}>Add Retailer</button>
        </div>

        <div className="application-admin-table-content">
          <BootstrapTable
            keyField = "id"
            data={retailers}
            columns={columns}
            rowClasses="datatable-action-row"
          />
        </div>

        <AddRetailerModal show={addRetailerModal} onHide={() => setAddRetailerModal(false)} updateRetailerslisting={() => setRetailersListing()} />
        <EditRetailerModal show={editModalVisible} onHide={() => toggleEditRetailerModal()} toggleeditmodal={() => toggleEditRetailerModal()} editdata={editModelData} updateRetailerslisting={() => setRetailersListing()} />

        {loading && <EziLoader />}
      </section>
    </React.Fragment>
  )
}
export default Dashboard; 