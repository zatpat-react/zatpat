import React from "react";
import { Modal } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';

function AddRetailerModal(props) {
    const { register, handleSubmit, errors } = useForm();

    const submitFormData = data => {
        const formData = new FormData();
        formData.append('name', data.name);
        formData.append('email', data.email);
        formData.append('mobile', data.mobile);
        formData.append('aadhar_no', data.aadhar_no);
        formData.append('details', data.details);
        formData.append('status', data.status); 
        Axios.post(configURL.RETAILERS, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                toast.success(res.data.message);
                props.onHide();
                props.updateRetailerslisting();
            } else {
                toast.error(res.data.message);

            }
        })
    };
    return (
        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="ezi-common-modal-wrapper" >
            <Modal.Body>
                <div className="application-admin-modal-header">
                    <span className="ezi-common-modal-body-close" onClick={props.onHide}></span>
                    <div className="admin-modal-header-text">
                        <span className="title">Add New Wholseller</span>
                    </div>
                </div>
                <form onSubmit={handleSubmit(submitFormData)} className="application-admin-form">
                    <div className="ezi-material-group">
                        <input type="text" name="name" id="name" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">Name</label>
                    </div>

                    <div className="ezi-material-group">
                        <input type="email" name="email" id="email" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">Email</label>
                    </div>

                    <div className="ezi-material-group">
                        <input type="text" name="mobile" id="mobile" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">Mobile</label>
                    </div>

                    <div className="ezi-material-group">
                        <input type="text" name="aadhar_no" id="aadhar_no" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">UID</label>
                    </div>

                    <div className="ezi-material-group">
                        <input type="text" name="details" id="details" className="ezi-material-input" ref={register({ required: true })} required /><span className="ezi-material-bar"></span>
                        <label className="ezi-material-label">Address</label>
                    </div>

                    <label> Status </label>
                        <select name="status" id="status" ref={register({ required: true })} required >
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </select>

                    <div className="application-admin-form-footer">
                        <input type="submit" value="Save" className="btn-ripple ezi-pink-btn application-admin-save-btn" />
                        <button type="button" className="application-admin-close-btn" onClick={props.onHide}>Close</button>
                    </div>
                </form>

            </Modal.Body>
        </Modal>
    );
}
export default AddRetailerModal;