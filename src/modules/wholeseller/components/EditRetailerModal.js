import React, { useState, useEffect } from "react";
import { Modal } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';

function EditRetailerModal(props) {
    const { register, handleSubmit, errors } = useForm();
    const [editData, setEditData] = useState({});

    useEffect(() => {
        setEditData(props.editdata)
    }, [props.show])

    const sendUpdatedData = () => {
        let updateData = { ...editData };
        let formData = new FormData();
        formData.append('name', updateData.name);
        formData.append('email', updateData.email);
        formData.append('mobile', updateData.mobile);
        formData.append('aadhar_no', updateData.aadhar_no);
        formData.append('details', updateData.details);
        formData.append('status', updateData.status);
        Axios.patch(configURL.RETAILERS+'/'+updateData.retailer_id, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                alert("User Edited Successfully.");
                props.updateRetailerslisting();
            } else {
                toast.warn(res.data.message || 'Something went wrong.')
            }
        })
    }
    const changeModalFormData = (e) => {
        let tempEditData = { ...editData };
        tempEditData[e.target.name] = e.target.value;
        setEditData({ ...tempEditData });
    }
    return (
        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="ezi-common-modal-wrapper" >
            <Modal.Body>
            <div className="application-admin-modal-header">
                    <span className="ezi-common-modal-body-close" onClick={props.onHide}></span>
                    <div className="admin-modal-header-text">
                        <span className="title">Edit New Wholseller</span>
                    </div>
                </div>
                <form className="application-admin-form">

                    <div class="ezi-material-group">
                        <input type="text" name="name" id="name" value={editData.name} className="ezi-material-input" required onChange={(e) => changeModalFormData(e)}/><span class="ezi-material-bar"></span>
                        <label className="ezi-material-label">Name</label>
                    </div>
                    <div class="ezi-material-group">
                        <input type="text" name="email" id="email" value={editData.email} className="ezi-material-input" required onChange={(e) => changeModalFormData(e)} /><span class="ezi-material-bar"></span>
                        <label className="ezi-material-label">Email</label>
                    </div>
                    <div class="ezi-material-group">
                        <input type="text" name="mobile" id="mobile" value={editData.mobile} className="ezi-material-input" required onChange={(e) => changeModalFormData(e)} /><span class="ezi-material-bar"></span>
                        <label className="ezi-material-label">Mobile</label>
                    </div>

                    <div class="ezi-material-group">
                        <input type="text" name="aadhar_no" id="aadhar_no" value={editData.aadhar_no}  className="ezi-material-input" required onChange={(e) => changeModalFormData(e)} /><span class="ezi-material-bar"></span>
                        <label className="ezi-material-label">UID</label>
                    </div>

                    <div class="ezi-material-group">
                        <input type="text" name="details" id="details" className="ezi-material-input" value={editData.details}  required onChange={(e) => changeModalFormData(e)} /><span class="ezi-material-bar"></span>
                        <label className="ezi-material-label">Address</label>
                    </div>

                    <label> Status </label>
                        <select name="status" id="status" ref={register({ required: true })} required   value={editData.status} onChange={(e) => changeModalFormData(e)} >
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </select>

                    <div className="application-admin-form-footer">
                    <button type="button" className="close-theme-btn" onClick={props.toggleeditmodal}>Close</button>
                    <button type="button" className="btn-ripple ezi-pink-btn add-theme-btn" onClick={() => sendUpdatedData()}>Update</button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
export default EditRetailerModal;