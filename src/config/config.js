const API_URL = 'http://127.0.0.1:8080/api/';
const configURL = {
    loginURL: `${API_URL}v1/users/login`,
    WHOLESELLERS: `${API_URL}v1/wholesellers`,
    RETAILERS: `${API_URL}v1/retailers`,
    TAGSLIST: `${API_URL}v1/tag-transactions/wholeseller-tags-count`,
    TAGREQUEST: `${API_URL}v1/tag-requests`,
    ASSIGNEDTAGLIST: `${API_URL}v1/retailers/statics`,
}

export default configURL;
